# My Electrum Install on Linux Debian

## Install

Full instructions here : https://electrum.org/#download

```
apt update && apt install -y wget python3-pyqt5 libsecp256k1-0 python3-cryptography python3-zbar
wget https://download.electrum.org/4.3.2/Electrum-4.3.2.tar.gz
tar -xvf Electrum-4.3.2.tar.gz
```

## Launch

```
python3 Electrum-4.3.2/run_electrum
```

## Recover a Wallet

Auto connect, Next

Next

Standard wallet, Next

I already have a seed, Next

enter the seed, Next

choose a random password, Next

## Configure

Tools > Preferences, Base unit : BTC

View > Show Addresses
